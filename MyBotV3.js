const hlt = require('./hlt');
const { Direction, Position } = require('./hlt/positionals');
const { Goal } = require('./hlt/goal');
const { MissionsEnum, SituationsEnum } = require('./hlt/enums');
const logging = require('./hlt/logging');

const game = new hlt.Game();
const EXPLORATION_MAG = 5;
const EARLY_GAME_MOVES = 30;
const STILL_MAX = 3;
const HALITE_GOAL = 0.80;
const MAX_SHIPS = 15;

var goalDictionary = new Array();  // of Goals
var nextDirection = new Direction(0, -1);
var situation = "";
var destinationsArray;

game.initialize().then(async () => {
    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.
    await game.ready('Bob');

    logging.info(`My Player ID is ${game.myId}. Shipyard: ${game.me.shipyard.position}`);
    destinationsArray = calculateBestDestinationsFromShipyard(game.gameMap, game.me.shipyard);

    while (true) {
        await game.updateFrame();

        const { gameMap, me } = game;
        const commandQueue = [];

        if (game.turnNumber <= EARLY_GAME_MOVES) {
            logging.info(`early game, turn:  ${game.turnNumber}`);
            situation = SituationsEnum.early;
        } 
        else if (game.turnNumber > EARLY_GAME_MOVES && game.turnNumber < hlt.constants.MAX_TURNS * 0.75) {
            logging.info(`midgame, turn: ${game.turnNumber}`);
            situation = SituationsEnum.mid;
        }
        else if (game.turnNumber > hlt.constants.MAX_TURNS * 0.75) {
            logging.info(`endgame, turn: ${game.turnNumber}`);
            situation = SituationsEnum.end;
            //goalDictionary = null;
            // clearDestinations();
        } else {
            logging.info("else situation");
            situation = SituationsEnum.early;
        }
        //logging.info(`goalDictionary: ${JSON.stringify(goalDictionary)}`);

        for (const ship of me.getShips()) {
            var shipGoal = goalDictionary[ship.id];
            //logging.info(`Ship ${ship.id} - shipGoal: ${JSON.stringify(shipGoal)}`);
            if (shipGoal != null) {
                if ((shipGoal.destination != null)) {
                    logging.info(`Ship ${ship.id} has destination: ${shipGoal.destination.toString()} - position: ${ship.position.toString()}`);
                    if (shipGoal.destination.toString() == ship.position.toString() && 
                        ship.position.toString() != me.shipyard.position.toString()) {
                        logging.info(`Ship ${ship.id} has arrived at destination ${ship.position.toString()}.  Gathering Halite`);
                        goalDictionary[ship.id] = new Goal(null, MissionsEnum.gather);
                    }
                    else if (ship.position.toString() == me.shipyard.position.toString()) {
                        logging.info(`Ship ${ship.id} has arrived at SHIPYARD.  Getting new Goal.`);
                        const goal = getGoal(ship,gameMap, me.shipyard.position);
                        goalDictionary[ship.id] = goal;
                        var move = gameMap.naiveNavigate(ship, goal.destination);
                        commandQueue.push(ship.move(move));
                        logging.info(`Ship ${ship.id} - Got goal - moving to ${goal.destination}`);
                    }
                    else {
                        const dest = gameMap.naiveNavigate(ship, shipGoal.destination);
                        if (dest.toString() == new Direction(0,0).toString()) {
                            const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
                            const destination = ship.position.directionalOffset(direction);
                            const safeMove = gameMap.naiveNavigate(ship, destination);
                            commandQueue.push(ship.move(safeMove));
                        } else {
                            commandQueue.push(ship.move(dest));
                        }
                        
                        logging.info(`Ship ${ship.id} moved to the   ${dest.toString()}`);
                    }
                    
                }
                else if (shipGoal.mission == MissionsEnum.gather) {
                    logging.info(`Ship ${ship.id} - gather mission`);
                    if (gameMap.get(ship.position).haliteAmount > 200 && ship.haliteAmount != hlt.constants.MAX_HALITE) {
                        commandQueue.push(ship.stayStill());
                    }
                    else {
                        goalDictionary[ship.id] = new Goal(me.shipyard.position, MissionsEnum.none);
                        commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
                        logging.info(`Ship ${ship.id} - Done with Gathering Mission.  Heading to shipyard`);
                    }
                }
                else if (ship.haliteAmount > HALITE_GOAL * hlt.constants.MAX_HALITE || situation == SituationsEnum.end){
                    logging.info(`Ship ${ship.id} - got enough halite - go to shipyard`);
                    commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
                }
                else if (ship.haliteAmount < hlt.constants.MAX_HALITE &&
                         gameMap.get(ship.position).haliteAmount > 400) {
                    logging.info(`Ship ${ship.id} - staying here to gather ${gameMap.get(ship.position).haliteAmount} halite`);
                }
                else {
                    logging.info(`Ship ${ship.id} else`);
                    var destination = findHighestHalite(ship.position, gameMap, 1, ship.haliteAmount);
                    if (destination == ship.position.toString()) {
                        try {
                            if (goalDictionary[ship.id].stillCount >= STILL_MAX) {
                                goalDictionary[ship.id].stillCount = 0;
                                const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
                                const destination = ship.position.directionalOffset(direction);
                                const safeMove = gameMap.naiveNavigate(ship, destination);
                                commandQueue.push(ship.move(safeMove));
                            } else {
                                goalDictionary[ship.id].addStill();
                                commandQueue.push(ship.stayStill());
                            }
                        }
                        catch (err) {
                            goalDictionary[ship.id].stillCount = 1;
                            commandQueue.push(ship.stayStill());
                        }
                    } else {
                        const safeMove = gameMap.naiveNavigate(ship, destination);
                        commandQueue.push(ship.move(safeMove));
                        goalDictionary[ship.id].destination = destination;
                        logging.info(`Ship ${ship.id} moved to the highest halite to the    ${safeMove.toWireFormat()}`);
                    }
                }
            } else {
                const goal = getGoal(ship,gameMap, me.shipyard.position);
                goalDictionary[ship.id] = goal;
                var move = gameMap.naiveNavigate(ship, goal.destination);
                commandQueue.push(ship.move(move));
                logging.info(`Ship ${ship.id} - Got goal - moving to ${goal.destination}`);
            }
        }

        if (game.turnNumber < 0.60 * hlt.constants.MAX_TURNS &&
            me.haliteAmount >= hlt.constants.SHIP_COST &&

            !gameMap.get(me.shipyard).isOccupied) {
            logging.info('create ship');
            commandQueue.push(me.shipyard.spawn());
        }

        await game.endTurn(commandQueue);
    }
});

function getNewExplorationDirection(curDirection) {
    logging.info(`getnewexplorationdirection: ${curDirection}`);
    if (curDirection.toWireFormat() == Direction.North.toWireFormat()) {
        nextDirection = Direction.East;
        return new Direction(0,(EXPLORATION_MAG * -1));
    } 
    else if (curDirection.toWireFormat() == Direction.East.toWireFormat()) {
        nextDirection = Direction.South;
        return new Direction(0, EXPLORATION_MAG);
    }
    else if (curDirection.toWireFormat() == Direction.South.toWireFormat()) {
        nextDirection = Direction.West;
        return new Direction(EXPLORATION_MAG,0);
    } 
    else if (curDirection.toWireFormat() == Direction.West.toWireFormat()) {
        nextDirection = Direction.North;
        return new Direction((EXPLORATION_MAG * -1),0);
    } 
    else {
        logging.info(`getNewExplorationDirection else : ${curDirection.toWireFormat()}`);
    }
}

function findHighestHalite(position, gameMap, magnitude, currentHaliteAmount) {
    //logging.info(`Find Highest Energy.  Position: ${position.x} ${position.y} `);
    var mostHalite = gameMap.get(position).haliteAmount;
    var mostHalitePos = position;
    for (i = 1; i <= magnitude; i++) {
        var halite = gameMap.get(new Position(position.x - i, position.y)).haliteAmount;
        //logging.info(`halite amount at ${position.x - i}, ${position.y}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x - i, position.y);
        }

        halite = gameMap.get(new Position(position.x - i, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x - i}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x - i, position.y - i);
        }

        halite = gameMap.get(new Position(position.x, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x, position.y - i);
        }

        halite = gameMap.get(new Position(position.x + i, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x + i}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x + i, position.y - i);
        }

        halite = gameMap.get(new Position(position.x + i, position.y)).haliteAmount;
        //logging.info(`halite amount at ${position.x + i}, ${position.y}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x + i, position.y);
        }

        halite = gameMap.get(new Position(position.x + i, position.y + i)).haliteAmount;
        //logging.info(`halite amount at ${position.x + i}, ${position.y + i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x + i, position.y + i);
        }

        halite = gameMap.get(new Position(position.x, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x, position.y - i);
        }

        halite = gameMap.get(new Position(position.x, position.y + i)).haliteAmount;
        //logging.info(`halite amount at ${position.x}, ${position.y + i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x, position.y + i);
        }

        halite = gameMap.get(new Position(position.x - i, position.y + i)).haliteAmount;
        //logging.info(`halite amount at ${position.x - i}, ${position.y + i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x - i, position.y + i);
        }
    }

    if ((mostHalite * 0.25) > (currentHaliteAmount * 0.10)) {
        logging.info(`most halite is at ${mostHalitePos}`);
        return mostHalitePos;
    }
    else {
        logging.info(`better to stay here at  ${position}.  Mosthalite: ${mostHalite} - currentHaliteAmount: ${currentHaliteAmount}`);
        return position;
    }
}

function calculateBestDestinationsFromShipyard(gameMap, shipyard) {
    var x = 0,y = 0;
    logging.info(`Calculating Best Destinations from Shipyard.  Dimensions: ${gameMap.width}, ${gameMap.height} . (x, y): ${x},${y}`);

    var tempArray = new Array(gameMap.height);
    var destArray = new Array();
    for (i = 0; i < gameMap.height; i++) {
        tempArray[i] = new Array(gameMap.width);
        for (j = 0; j < gameMap.width; j++) {
            var pos = new Position(j,i);
            var ha = gameMap.get(pos).haliteAmount;
            var distance = gameMap.calculateDistance(shipyard.position, pos) * 2; // times 2 to go there and back? 
            var eff = (ha - Math.pow(ha * 0.10,distance));

            tempArray[i][j] = eff;
            destArray.push({
                position: pos,
                eff: eff
            });
            logging.info(`${pos} - halite: ${ha}. distance: ${distance}. eff: ${eff}. (j,i): ${j},${i}`);
        }
    }
    
    destArray = destArray.sort((a,b) => a.eff - b.eff);
    // logging.info(`${JSON.stringify(destArray)}`);
    return destArray;    
}

function clearDestinations() {
    for(i = 0; i < goalDictionary.length; i++) {
        try {
            goalDictionary[i].destination = null;
        }
        catch (err) {
        }
    }
}

function getGoal(ship, gameMap, shipyardPosition) {    
    if (situation == SituationsEnum.early || situation == SituationsEnum.mid) {
        if (ship.position.toString() == shipyardPosition.toString()) {            
            const dest = destinationsArray.pop().position;
            logging.info(`Ship ${ship.id} - GetGoal - Early/Mid - At Shipyard - Popping new Destination from Array: ${dest}`);
            return new Goal(dest,MissionsEnum.none);
        } 
        else {
            //calculate distance to see if we should go to the shipyard or head back to
            // const distToHome = gameMap.calculateDistance(ship.position, me.shipyard.position.toString());
            
            if (ship.haliteAmount > 400) {
                logging.info(`Ship ${ship.id} - GetGoal - Early/Mid - Got more than 400 Halite - Going to shipyard`);
                return new Goal(shipyardPosition, MissionsEnum.none);
            }
            else {
                const dest = destinationsArray.pop().position;
                logging.info(`Ship ${ship.id} - GetGoal - Early/Mid - Got more than 400 Halite - Going to shipyard`);
                return new Goal(dest, MissionsEnum.none);
            }
        }
    }    
    else if (situation == SituationsEnum.end) {
        if (ship.position.toString() == shipyardPosition.toString()) {
            const dest = findHighestHalite(ship.position, gameMap, 5, ship.haliteAmount);
            logging.info(`Ship ${ship.id} - GetGoal - End - At Shipyard - Get halite close to source: ${dest}`);
            return new Goal(dest, MissionsEnum.none);
        }
        else {
            logging.info(`Ship ${ship.id} - GetGoal - End - Time to head back - Going to shipyard`);
            return new Goal(shipyardPosition, MissionsEnum.none);
        }
    }
    else {
        logging.warn(`GetGoal ELSE STATEMENT`);
    }
}