class Goal {
    constructor(destination, mission) {
        this.destination = destination;
        this.stillCount = 0;
        this.mission = mission;
    }

    toString() {
        return `${this.constructor.name} `
    }

    addStill() {
        this.stillCount = this.stillCount + 1;
    }

    setDestination(pos) {
        this.destination = pos;
    }

    hasArrivedAtDestination(pos) {
        if (this.destination.toString() === pos.toString()) {
            return true;
        } else false;
    }
}

module.exports = {
    Goal
};