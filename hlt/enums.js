const MissionsEnum = Object.freeze({
    "none": 0,
    "gather": 1
});

const SituationsEnum = Object.freeze({
    "early": 0,
    "mid": 1,
    "end": 2
});

module.exports = {
    MissionsEnum,
    SituationsEnum
};