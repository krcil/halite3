const hlt = require('./hlt');
const { Direction } = require('./hlt/positionals');
const logging = require('./hlt/logging');

const game = new hlt.Game();
var nextDirection = new Direction(0, -1);
const explorationMag = 5;

game.initialize().then(async () => {
    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.
    await game.ready('MyJavaScriptBot');

    logging.info(`My Player ID is ${game.myId}.`);

    var goalDictionary = new Array();
    var situation = "";

    while (true) {
        await game.updateFrame();

        const { gameMap, me } = game;
        const commandQueue = [];

        if (game.turnNumber <= 10) {
            logging.info(`early game, turn:  ${game.turnNumber}`);
            situation = "early";
        } 
        else if (game.turnNumber > 10 && game.turnNumber < hlt.constants.MAX_TURNS * 0.75) {
            logging.info(`midgame, turn: ${game.turnNumber}`);
            situation = "midgame";
        }
        else if (game.turnNumber > hlt.constants.MAX_TURNS * 0.75) {
            logging.info(`endgame, turn: ${game.turnNumber}`);
            situation = "endgame";
        } else {
            logging.info("else situation");
            situation = "";
        }
        //logging.info(`goalDictionary: ${JSON.stringify(goalDictionary)}`);

        for (const ship of me.getShips()) {
            //see if we have a goal
            var shipGoal = goalDictionary[ship.id-1];
            logging.info(`shipGoal: ${JSON.stringify(shipGoal)}`);
            if (shipGoal != null) {
                if ((shipGoal.destination != null)) {
                    logging.info(`ship ${ship.id} has destination: ${shipGoal.destination.toString()} - position: ${ship.position.toString()}`);
                    if (shipGoal.destination.toString() == ship.position.toString()) {
                        const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
                        const destination = ship.position.directionalOffset(direction);
                        const safeMove = gameMap.naiveNavigate(ship, destination);
                        commandQueue.push(ship.move(safeMove));
                        logging.info(`ship ${ship.id} randomly moved to the ${safeMove.toString()} - ${safeMove.toWireFormat()}`);
                    } 
                    else {
                        const dest = gameMap.naiveNavigate(ship, shipGoal.destination);
                        commandQueue.push(ship.move(dest));
                        logging.info(`ship ${ship.id} moved to the ${dest.toString()}`);
                    }
                    
                }
                else if ((shipGoal.goal == null) && (shipGoal.destination == null) && (shipGoal.direction != null)) {
                    logging.info(`ship ${ship.id} has direction goal: ${shipGoal.direction}`);
                    commandQueue.push(ship.move(shipGoal.direction));
                } 
                else {
                    logging.info(`ship ${ship.id} else `);
                    commandQueue.push(ship.move(Direction.Still));
                }
            }
            else if (situation == "early") {
                const dir = getNewExplorationDirection(nextDirection);
                const dest = ship.position.directionalOffset(dir)               
                goalDictionary.push({
                    goal: null,
                    destination: dest,
                    direction: null
                });
                logging.info(`early game for shipId: ${ship.id} - Position ${ship.position.toString()} - Direction:  ${dir} - destination: ${dest}`);
                commandQueue.push(ship.move(gameMap.naiveNavigate(ship,ship.position.directionalOffset(dir))));
            }
            // else if (situation == "midgame") {
                
            // }
            else if (situation == "endgame") {
                logging.info(`endgame move for ship ${ship.id}`);
                commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
            }
            else if (ship.haliteAmount > 0.80 * hlt.constants.MAX_HALITE) {
                logging.info(`almost full on halite ${ship.id}`);
                commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
            }
            else if ((gameMap.get(ship.position) > 1) && (ship.haliteAmount < hlt.constants.MAX_HALITE)) {
                logging.info(`gather halite ${ship.id}`);
                commandQueue.push(ship.stayStill());
            }
            // else if (ship.haliteAmount > hlt.constants.MAX_HALITE / 2) {
            //     const destination = me.shipyard.position;
            //     const safeMove = gameMap.naiveNavigate(ship, destination);
            //     commandQueue.push(ship.move(safeMove));
            // }
            else if (gameMap.get(ship.position).haliteAmount < hlt.constants.MAX_HALITE / 10) {
                logging.info(`go in random direction Ship ${ship.id}`);
                const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
                const destination = ship.position.directionalOffset(direction);
                const safeMove = gameMap.naiveNavigate(ship, destination);
                commandQueue.push(ship.move(safeMove));
            }

            if (game.turnNumber < 0.50 * hlt.constants.MAX_TURNS &&
                me.haliteAmount >= hlt.constants.DROPOFF_COST &&
                gameMap.calculateDistance(gameMap.get(ship.position),me.shipyard.position) > 6) {
                logging.info(`creating dropoff ship ${ship.id} .  Distance: ${gameMap.calculateDistance(gameMap.get(ship.position),me.shipyard.position).toString()}`);
                commandQueue.push(me.ship.makeDropoff());
            }
        }

        if (game.turnNumber < 0.75 * hlt.constants.MAX_TURNS &&
            me.haliteAmount >= hlt.constants.SHIP_COST &&
            !gameMap.get(me.shipyard).isOccupied) {
            logging.info('create ship');
            commandQueue.push(me.shipyard.spawn());
        }

        await game.endTurn(commandQueue);
    }
});

function getNewExplorationDirection(curDirection) {
    logging.info(`getnewexplorationdirection: ${curDirection}`);
    if (curDirection.toWireFormat() == Direction.North.toWireFormat()) {
        nextDirection = Direction.East;
        return new Direction(0,(explorationMag * -1));
    } 
    else if (curDirection.toWireFormat() == Direction.East.toWireFormat()) {
        nextDirection = Direction.South;
        return new Direction(0, explorationMag);
    }
    else if (curDirection.toWireFormat() == Direction.South.toWireFormat()) {
        nextDirection = Direction.West;
        return new Direction(explorationMag,0);
    } 
    else if (curDirection.toWireFormat() == Direction.West.toWireFormat()) {
        nextDirection = Direction.North;
        return new Direction((explorationMag * -1),0);
    } 
    else {
        logging.info(`getNewExplorationDirection else : ${curDirection.toWireFormat()}`);
    }

    // var directions = ["n", "e", "s", "w"];
    // var cur = directions.indexOf(curDirection);
    // if ((cur + 1) > (directions.length - 1)){
    //     return "n"
    // } else {
    //     return directions[cur + 1];
    // }
}