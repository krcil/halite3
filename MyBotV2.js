const hlt = require('./hlt');
const { Direction, Position } = require('./hlt/positionals');
const { Goal } = require('./hlt/goal');
const logging = require('./hlt/logging');

const game = new hlt.Game();
const EXPLORATION_MAG = 5;
const EARLY_GAME_MOVES = 30;
const STILL_MAX = 3;
const HALITE_GOAL = 0.50;
const MAX_SHIPS = 15;

var goalDictionary = new Array();
var nextDirection = new Direction(0, -1);
var situation = "";

game.initialize().then(async () => {
    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.
    await game.ready('Joey');

    logging.info(`My Player ID is ${game.myId}.`);

    while (true) {
        await game.updateFrame();

        const { gameMap, me } = game;
        const commandQueue = [];

        if (game.turnNumber <= EARLY_GAME_MOVES) {
            logging.info(`early game, turn:  ${game.turnNumber}`);
            situation = "early";
        } 
        else if (game.turnNumber > EARLY_GAME_MOVES && game.turnNumber < hlt.constants.MAX_TURNS * 0.75) {
            logging.info(`midgame, turn: ${game.turnNumber}`);
            situation = "midgame";
        }
        else if (game.turnNumber > hlt.constants.MAX_TURNS * 0.75) {
            logging.info(`endgame, turn: ${game.turnNumber}`);
            situation = "endgame";
        } else {
            logging.info("else situation");
            situation = "";
        }
        logging.info(`goalDictionary: ${JSON.stringify(goalDictionary)}`);

        for (const ship of me.getShips()) {
            var shipGoal = goalDictionary[ship.id];
            //logging.info(`Ship ${ship.id} - shipGoal: ${JSON.stringify(shipGoal)}`);
            if (shipGoal != null) {
                if ((shipGoal.destination != null)) {
                    logging.info(`Ship ${ship.id} has destination: ${shipGoal.destination.toString()} - position: ${ship.position.toString()}`);
                    if (shipGoal.destination.toString() == ship.position.toString()) {
                        var destination = findHighestHalite(ship.position, gameMap, 1);
                        logging.info(`Ship ${ship.id} has arrived at destination ${ship.position.toString()}.  New Destination: ${destination}`);
                        
                        if (destination == ship.position.toString()) {
                            goalDictionary[ship.id].stillCount = 1;
                            goalDictionary[ship.id].destination = null;
                            commandQueue.push(ship.stayStill());
                            logging.info(`Ship ${ship.id} has stayed still`)
                        }
                        else {
                            const safeMove = gameMap.naiveNavigate(ship, destination);
                            commandQueue.push(ship.move(safeMove));
                            // shipGoal.destination = destination;
                            // goalDictionary[ship.id - 1] = shipGoal;
                            goalDictionary[ship.id].destination = destination;
                            logging.info(`Ship ${ship.id} moved to the highest halite to the    ${safeMove.toWireFormat()}`);
                        }
                    } 
                    else {
                        const dest = gameMap.naiveNavigate(ship, shipGoal.destination);
                        if (dest.toString() == new Direction(0,0).toString()) {
                            const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
                            const destination = ship.position.directionalOffset(direction);
                            const safeMove = gameMap.naiveNavigate(ship, destination);
                            commandQueue.push(ship.move(safeMove));
                        } else {
                            commandQueue.push(ship.move(dest));
                        }
                        
                        logging.info(`Ship ${ship.id} moved to the   ${dest.toString()}`);
                    }
                    
                }
                else if (ship.haliteAmount > HALITE_GOAL * hlt.constants.MAX_HALITE){
                    logging.info(`Ship ${ship.id} - got enough halite - go to shipyard`);
                    commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
                }
                else {
                    logging.info(`Ship ${ship.id} else `);
                    var destination = findHighestHalite(ship.position, gameMap, 1);
                    if (destination == ship.position.toString()) {
                        try {
                            if (goalDictionary[ship.id].stillCount >= STILL_MAX) {
                                goalDictionary[ship.id].stillCount = 0;
                                const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
                                const destination = ship.position.directionalOffset(direction);
                                const safeMove = gameMap.naiveNavigate(ship, destination);
                                commandQueue.push(ship.move(safeMove));
                            } else {
                                goalDictionary[ship.id].addStill();
                                commandQueue.push(ship.stayStill());
                            }
                        }
                        catch (err) {
                            goalDictionary[ship.id].stillCount = 1;
                            commandQueue.push(ship.stayStill());
                        }
                    } else {
                        const safeMove = gameMap.naiveNavigate(ship, destination);
                        commandQueue.push(ship.move(safeMove));
                        goalDictionary[ship.id].destination = destination;
                        logging.info(`Ship ${ship.id} moved to the highest halite to the    ${safeMove.toWireFormat()}`);
                    }
                }
            } else {
                if (situation == "early") {
                    const dir = getNewExplorationDirection(nextDirection);
                    const dest = ship.position.directionalOffset(dir);
                    goalDictionary[ship.id] = new Goal(dest);
                    logging.info(`Ship ${ship.id} - early game - Position ${ship.position.toString()} - Direction:  ${dir} - destination: ${dest}`);
                    commandQueue.push(ship.move(gameMap.naiveNavigate(ship, ship.position.directionalOffset(dir))));
                } 
                else if (situation == "endgame")
                {
                    logging.info(`Ship ${ship.id} - endgame move - go to shipyard`);
                    commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
                }
                else {
                    logging.info(`ship ${ship.id} - else situation.`);

                    // if ((gameMap.get(ship.position).haliteAmount * 0.25) > ship.haliteAmount )
                    var destination = findHighestHalite(ship.position, gameMap, 1);
                    if (destination == ship.position.toString()) {
                        try {
                            if (goalDictionary[ship.id].stillCount >= STILL_MAX) {
                                goalDictionary[ship.id].stillCount = 0;
                                const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
                                const destination = ship.position.directionalOffset(direction);
                                const safeMove = gameMap.naiveNavigate(ship, destination);
                                commandQueue.push(ship.move(safeMove));
                            } else {
                                goalDictionary[ship.id].addStill();
                                commandQueue.push(ship.stayStill());
                            }
                        }
                        catch (err) {
                            goalDictionary[ship.id] = new Goal(null);
                            goalDictionary[ship.id].addStill();
                            commandQueue.push(ship.stayStill());
                        }
                    } else {
                        const safeMove = gameMap.naiveNavigate(ship, destination);
                        commandQueue.push(ship.move(safeMove));
                        if (goalDictionary[ship.id] == null) {
                            goalDictionary[ship.id] = new Goal(destination);
                        } 
                        else {
                            goalDictionary[ship.id].destination = destination;
                        }
                        logging.info(`Ship ${ship.id} moved to the highest halite to the    ${safeMove.toWireFormat()} with destionation of ${destination}`);
                    }
                }
            }
            // else if (situation == "early") {
            //     const dir = getNewExplorationDirection(nextDirection);
            //     const dest = ship.position.directionalOffset(dir);
            //     goalDictionary[ship.id].destination = dest;
            //     logging.info(`Ship ${ship.id} - early game - Position ${ship.position.toString()} - Direction:  ${dir} - destination: ${dest}`);
            //     commandQueue.push(ship.move(gameMap.naiveNavigate(ship,ship.position.directionalOffset(dir))));
            // }
            // // else if (situation == "midgame") {
                
            // // }
            // else if (situation == "endgame") {
            //     logging.info(`Ship ${ship.id} - endgame move - go to shipyard`);
            //     commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
            // }
            // else if (ship.haliteAmount > 0.80 * hlt.constants.MAX_HALITE) {
            //     logging.info(`Ship ${ship.id} - almost full on halite`);

            //     goalDictionary[ship.id] = new Goal(me.shipyard.position);
            //     commandQueue.push(ship.move(gameMap.naiveNavigate(ship, me.shipyard.position)));
            // }
            // else if ((gameMap.get(ship.position) > 1) && (ship.haliteAmount < hlt.constants.MAX_HALITE)) {
            //     logging.info(`Ship  ${ship.id} - gather halite`);
            //     try {
            //         var cnt = goalDictionary[ship.id].stillCount;
            //         if (cnt >= STILL_MAX) {
            //             goalDictionary[ship.id].stillCount = null;
            //             const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
            //             const destination = ship.position.directionalOffset(direction);
            //             const safeMove = gameMap.naiveNavigate(ship, destination);
            //             commandQueue.push(ship.move(safeMove));
            //         } else {
            //             goalDictionary[ship.id] = {
            //                 stillCount: 1
            //             };
            //             commandQueue.push(ship.stayStill());
            //         }
            //     }
            //     catch (err) {
            //         goalDictionary[ship.id] = {
            //             stillCount: 1
            //         };
            //         commandQueue.push(ship.stayStill());
            //     }
            // }
            // // else if (ship.haliteAmount > hlt.constants.MAX_HALITE / 2) {
            // //     const destination = me.shipyard.position;
            // //     const safeMove = gameMap.naiveNavigate(ship, destination);
            // //     commandQueue.push(ship.move(safeMove));
            // // }
            // else if (gameMap.get(ship.position).haliteAmount < hlt.constants.MAX_HALITE / 10) {
            //     logging.info(`Ship ${ship.id} - go get highest Halite`);
            //     // const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
            //     // const destination = ship.position.directionalOffset(direction);
            //     const destination = findHighestHalite(ship.position, gameMap, 1);
            //     if (ship.position.toString() == destination) {
            //         try {
            //             var cnt = goalDictionary[ship.id].stillCount;
            //             if (cnt >= STILL_MAX) {
            //                 goalDictionary[ship.id].stillCount = null;
            //                 const direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
            //                 const destination = ship.position.directionalOffset(direction);
            //                 const safeMove = gameMap.naiveNavigate(ship, destination);
            //                 commandQueue.push(ship.move(safeMove));
            //             } else {
            //                 goalDictionary[ship.id] = {
            //                     stillCount: 1
            //                 };
            //                 commandQueue.push(ship.stayStill());
            //             }
            //         }
            //         catch (err) {
            //             goalDictionary[ship.id] = {
            //                 stillCount: 1
            //             };
            //             commandQueue.push(ship.stayStill());
            //         }
            //     } else {
            //         const safeMove = gameMap.naiveNavigate(ship, destination);
            //         commandQueue.push(ship.move(safeMove));
            //     }
            // }

            // if (game.turnNumber < 0.50 * hlt.constants.MAX_TURNS &&
            //     me.haliteAmount >= hlt.constants.DROPOFF_COST &&
            //     gameMap.calculateDistance(gameMap.get(ship.position),me.shipyard.position) > 6) {
            //     logging.info(`creating dropoff ship ${ship.id} .  Distance: ${gameMap.calculateDistance(gameMap.get(ship.position),me.shipyard.position).toString()}`);
            //     commandQueue.push(me.ship.makeDropoff());
            // }
        }

        if (game.turnNumber < 0.75 * hlt.constants.MAX_TURNS &&
            me.haliteAmount >= hlt.constants.SHIP_COST &&
            !gameMap.get(me.shipyard).isOccupied) {
            logging.info('create ship');
            commandQueue.push(me.shipyard.spawn());
        }

        await game.endTurn(commandQueue);
    }
});

function getNewExplorationDirection(curDirection) {
    logging.info(`getnewexplorationdirection: ${curDirection}`);
    if (curDirection.toWireFormat() == Direction.North.toWireFormat()) {
        nextDirection = Direction.East;
        return new Direction(0,(EXPLORATION_MAG * -1));
    } 
    else if (curDirection.toWireFormat() == Direction.East.toWireFormat()) {
        nextDirection = Direction.South;
        return new Direction(0, EXPLORATION_MAG);
    }
    else if (curDirection.toWireFormat() == Direction.South.toWireFormat()) {
        nextDirection = Direction.West;
        return new Direction(EXPLORATION_MAG,0);
    } 
    else if (curDirection.toWireFormat() == Direction.West.toWireFormat()) {
        nextDirection = Direction.North;
        return new Direction((EXPLORATION_MAG * -1),0);
    } 
    else {
        logging.info(`getNewExplorationDirection else : ${curDirection.toWireFormat()}`);
    }
}

function findHighestHalite(position, gameMap, magnitude, currentHaliteAmount) {
    //logging.info(`Find Highest Energy.  Position: ${position.x} ${position.y} `);
    var mostHalite = gameMap.get(position).haliteAmount;
    var mostHalitePos = position;
    for (i = 1; i <= magnitude; i++) {
        var halite = gameMap.get(new Position(position.x - i, position.y)).haliteAmount;
        //logging.info(`halite amount at ${position.x - i}, ${position.y}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x - i, position.y);
        }

        halite = gameMap.get(new Position(position.x - i, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x - i}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x - i, position.y - i);
        }

        halite = gameMap.get(new Position(position.x, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x, position.y - i);
        }

        halite = gameMap.get(new Position(position.x + i, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x + i}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x + i, position.y - i);
        }

        halite = gameMap.get(new Position(position.x + i, position.y)).haliteAmount;
        //logging.info(`halite amount at ${position.x + i}, ${position.y}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x + i, position.y);
        }

        halite = gameMap.get(new Position(position.x + i, position.y + i)).haliteAmount;
        //logging.info(`halite amount at ${position.x + i}, ${position.y + i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x + i, position.y + i);
        }

        halite = gameMap.get(new Position(position.x, position.y - i)).haliteAmount;
        //logging.info(`halite amount at ${position.x}, ${position.y - i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x, position.y - i);
        }

        halite = gameMap.get(new Position(position.x, position.y + i)).haliteAmount;
        //logging.info(`halite amount at ${position.x}, ${position.y + i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x, position.y + i);
        }

        halite = gameMap.get(new Position(position.x - i, position.y + i)).haliteAmount;
        //logging.info(`halite amount at ${position.x - i}, ${position.y + i}: ${halite}`);
        if (halite > mostHalite) {
            mostHalite = halite;
            mostHalitePos = new Position(position.x - i, position.y + i);
        }

        if (mostHalite * 0.25 > currentHaliteAmount * 0.10) {
            logging.info(`most halite is at ${mostHalitePos}`);
            return mostHalitePos;
        }
        else {
            logging.info(`better to stay here at  ${position}`);
            return position;
        }
    }
}

function stillMoveCheck(ship, goalDictionary) {
    // try {
    //     goalDictionary[ship.id]
    // }
    // catch {        
    //     return 
    // }
}